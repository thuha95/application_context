package com.example.applicationcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String KEY_IMAGE = "101";
    public static final String KEY_INFOR = "102";
    public static final String KEY_NAME = "103";
    private View lnBee, lnDog, lnDuck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initEvents();
    }

    private void initEvents() {
        lnBee.setOnClickListener(this);
        lnDog.setOnClickListener(this);
        lnDuck.setOnClickListener(this);
    }

    private void initViews() {
        lnBee = findViewById(R.id.ln_bee);
        lnDog = findViewById(R.id.ln_dog);
        lnDuck = findViewById(R.id.ln_duck);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ln_bee:
                showAnimalInfor(R.drawable.ic_bee, R.string.bee_infor, "Bee");
                break;
            case R.id.ln_dog:
                showAnimalInfor(R.drawable.ic_dog, R.string.dog_infor, "Dog");
                break;
            case R.id.ln_duck:
                showAnimalInfor(R.drawable.ic_duck, R.string.duck_infor, "Duck");
                break;
            default:
                break;
        }
    }

    private void showAnimalInfor(int imageId, int inforId, String name) {
//        Intent intent = new Intent();
//        intent.setClass(this, DetailActivity.class);
//        intent.putExtra(KEY_IMAGE, imageId);
//        intent.putExtra(KEY_INFOR, inforId);
//        intent.putExtra(KEY_NAME, name);
//        startActivity(intent);

        MyApplication.getInstance().setIdImage(imageId);
        MyApplication.getInstance().setIdString(inforId);
        MyApplication.getInstance().setName(name);

        startActivity(new Intent(this, DetailActivity.class));

    }
}