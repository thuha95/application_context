package com.example.applicationcontext;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {
    private ImageView imvBack, imvAnimal;
    private TextView tvInfor, tvTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);

        initViews();
        initEvents();
        initData();
    }

    private void initData() {
//        Intent intent = getIntent();
//        int idImage = intent.getIntExtra(MainActivity.KEY_IMAGE, 0);
//        int idInfor = intent.getIntExtra(MainActivity.KEY_INFOR, 0);
//        String name = intent.getStringExtra(MainActivity.KEY_NAME);
//        tvTitle.setText(name);
//        imvAnimal.setImageResource(idImage);
//        tvInfor.setText(idInfor);

        tvTitle.setText(MyApplication.getInstance().getName());
        imvAnimal.setImageResource(MyApplication.getInstance().getIdImage());
        tvInfor.setText(MyApplication.getInstance().getIdString());

    }

    private void initEvents() {
        imvBack.setOnClickListener(v -> doClose());
    }

    private void doClose() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Anotation");
        dialog.setMessage("Do you want to exit?");
        dialog.setNegativeButton("Agree", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.show();
    }

    private void initViews() {
        imvAnimal = findViewById(R.id.imv_animal);
        tvInfor = findViewById(R.id.tv_infor);
        imvBack = findViewById(R.id.imv_back);
        tvTitle = findViewById(R.id.title);
    }
}
