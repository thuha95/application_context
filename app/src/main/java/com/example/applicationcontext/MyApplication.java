package com.example.applicationcontext;

import android.app.Application;

public class MyApplication extends Application {
    private static MyApplication instance;
    private int idImage;
    private int idString;
    private String name;

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    public void setIdString(int idString) {
        this.idString = idString;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdImage() {
        return idImage;
    }

    public int getIdString() {
        return idString;
    }

    public String getName() {
        return name;
    }

//    private MyApplication() {
//        //for singleton
//    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static MyApplication getInstance() {
        if(instance == null) {
            instance = new MyApplication();
        }
        return instance;
    }
}
